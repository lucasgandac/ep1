#include "candidato.hpp"
#include <string>
#include "pessoa.hpp"

using namespace std;

    Candidato::Candidato()
{
    partido = "";
    num_candidato = 0;
    cargo = "";
    num_partido = 0;
    num_ue = 0;
    sigla_partido = "";

}
    void Candidato::setPartido(string partido)
{
        this->partido= partido;
}   
    string Candidato::getPartido()
{
        return partido;
}
    void Candidato::setNum_candidato(int num_candidato)
{
        this->num_candidato= num_candidato;
}   
    int Candidato::getNum_candidato()
{
        return num_candidato;
}
    void Candidato::setCargo(string cargo)
{
        this->cargo= cargo;
}   
    string Candidato::getCargo()
{
        return cargo;
}
    void Candidato::setNum_partido(int num_partido)
{
        this->num_partido= num_partido;
}   
    int Candidato::getNum_partido()
{
        return num_partido;
}
    void Candidato::setNum_ue(int num_ue)
{
        this->num_ue= num_ue;
}   
    int Candidato::getNum_ue()
{
        return num_ue;
}
    void Candidato::setSigla_partido(string sigla_partido)
{
        this->sigla_partido= sigla_partido;
}   
    string Candidato::getSigla_partido()
{
        return sigla_partido;
}   
