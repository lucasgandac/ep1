#ifndef PESSOA_HPP
#define PESSOA_HPP
#include <bits/stdc++.h>

using namespace std;

class Pessoa 
{
    private:
        string nome;
        int cpf;

    public: 
        Pessoa();    
        ~Pessoa();
        
        string getNome();
        void setNome(string nome);

        int getCpf();
        void setCpf(int cpf);

     

};

#endif
