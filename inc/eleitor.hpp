#ifndef ELEITOR_HPP
#define ELEITOR_HPP
#include <bits/stdc++.h>
#include "pessoa.hpp"

using namespace std;

class Eleitor : public Pessoa
{
    private:
    int titulo_eleitoral;

    public:
    Eleitor();    
    ~Eleitor();
    
    int getTitulo_eleitoral();
    void setTitulo_eleitoral(int titulo_eleitoral);
};

#endif
