#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP
#include <bits/stdc++.h>    

using namespace std;

class Candidato
{
    private:
        string partido;
        int num_partido;
        int num_ue;
        string sigla_partido;
        int  num_candidato;
        string cargo;

    public: 
        Candidato();    
        ~Candidato();
        
        string getPartido();
        void setPartido(string partido);

        int getNum_partido();
        void setNum_partido(int num_partido);

        int getNum_ue();
        void setNum_ue(int num_ue);

        string getSigla_partido();
        void setSigla_partido(string sigla_partido);

        int getNum_candidato();
        void setNum_candidato(int num_candidato);

        string getCargo();
        void setCargo(string cargo);

};

#endif
